import paho.mqtt.client as paho
import json
from datetime import datetime


id_entrepot = 13

def on_connect(client, userdata, flags, rc):
    print("Connect : "+str(rc))

def on_message(client, userdata, msg):
    now = datetime.now()

    msg = msg.payload.decode()
    json_msg = json.loads(msg)

    date_str = now.strftime("%d/%m/%Y %H:%M:%S")
    json_msg["id_entrepot"] = id_entrepot
    json_msg["date"] = date_str

    print(str(json_msg))

    client.publish("serv", json.dumps(json_msg, indent=4))

if __name__ == '__main__':
    client = paho.Client()
    client.on_message = on_message
    client.connect("10.11.4.1",1883,60)
    client.subscribe("entrepot")
    client.loop_forever()