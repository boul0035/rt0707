import os, time, json
import paho.mqtt.client as paho

id_obj = "02"

def init_colis():
    msg = {"id_colis": id_obj, "code" : 1}
    pass


if __name__ == '__main__':
    msg = {"id_colis" : id_obj, "code" : 2}
    while(1):
        client = paho.Client()
        client.connect("localhost", 1883, 60)
        client.publish("entrepot", json.dumps(msg, indent=4))
        print("Message envoyé a la file")
        client.disconnect()
        time.sleep(5)